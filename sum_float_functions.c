//program to fin sum of two floats using functions.
#include<stdio.h>
float input()
{
    float a;
    scanf("%f",&a);
    return a;
}
float compute(float a,float b)
{
    float sum;
    sum=a+b;
    return sum;
}
void output(float sum)
{
    printf("The sum of two numbers is : %f",sum);
}
void main()
{
    float a,b,sum;
    printf("Enter two numbers: \n");
    a=input();
    b=input();
    sum=compute(a,b);
    output(sum);
}
