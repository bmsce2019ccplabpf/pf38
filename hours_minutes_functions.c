//program to calculate number of minutes using functions.
#include<stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
int compute(int a,int b)
{
    int sum=(a*60)+b;
    return sum;
}
void output(int sum)
{
    printf("The total number of MINUTES is : %d",sum);
}
void main()
{
    int a,b,sum;
    printf("Enter the number of HOURS: \n");
    a=input();
    printf("Enter the number of MINUTES: \n");
    b=input();
    sum=compute(a,b);
    output(sum);
}
