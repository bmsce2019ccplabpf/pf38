#include<stdio.h>
#include<math.h>
int  input()
{
	int a;
	scanf("%d",&a);
	return a;
}
int compute(int a,int b,int c,int d)
{
	int dis;
	dis = sqrt((a-c)*(a-c)+(b-d)*(b-d));
	return dis;
}
void output(int dis)
{
	printf("Distance is : %d",dis);
}
void main()
{
	int a,b,c,d,dis;
	printf("Enter the Co-ordinates of both points : \n");
	a=input();
	b=input();
	c=input();
	d=input();
	dis=compute(a,b,c,d);
	output(dis);
}