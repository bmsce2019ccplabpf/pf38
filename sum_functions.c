//program to fin sum of two integers using functions.
#include<stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
int compute(int a,int b)
{
    int sum;
    sum=a+b;
    return sum;
}
void output(int sum)
{
    printf("The sum of two numbers is : %d",sum);
}
void main()
{
    int a,b,sum;
    printf("Enter two numbers: \n");
    a=input();
    b=input();
    sum=compute(a,b);
    output(sum);
}
