//program to find square of a number using functions.
#include<stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
int compute(int a)
{
    int square;
    square=a*a;
    return square;
}
void output(int square)
{
    printf("The SQUARE of the given number is : %d",square);
}
void main()
{
    int a,b,square;
    printf("Enter the number to be SQUARED : \n");
    a=input();
    square=compute(a);
    output(square);
}
