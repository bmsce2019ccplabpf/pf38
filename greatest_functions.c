//program to find greatest of three numbers.
#include<stdio.h>
#include<math.h>
int input()
{
    int a;
    printf("Enter number : \n");
    scanf("%d",&a);
    return a;
}
int compute(int a,int b,int c)
{
    if(a>b&&a>c)
        return a;
    else if(b>a&&b>c)
        return b;
    else
        return c;
}
void output(int a)
{
    printf("Greatest of the given numbers is %d",a);
}
void main()
{
    printf("You have to enter three numbers \n");
    int a,b,c,g;
    a=input();
    b=input();
    c=input();
    g=compute(a,b,c);
    output(g);
}
