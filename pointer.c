#include<stdio.h>
int swap(int*m, int*n);
int main()
{
    int m,n;
    printf("m & n values please: \n");
    scanf("%d%d",&m,&n);
    swap(&m,&n);
    printf("The values after swapping of m and n are: \n");
    printf("m = %d \nn = %d",m,n);
    return 0;
}
int swap(int*m, int*n)
{
    int temp;
    temp = *m;
    *m = *n;
    *n = temp;
    return 0;
}
