#include<stdio.h>
int input()
{
    int a;
    scanf("%d",&a);
    return a;
}
int compute(int a,int b)
{
    int sum = a+b;
    return sum;
}
void output(int sum)
{
    printf("Sum of given two numbers = %d",sum);
}
void main()
{
    printf("Enter two numbers : \n");
    int a = input();
    int b = input();
    int sum = compute(a,b);
    output(sum);
}
