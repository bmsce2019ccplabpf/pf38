#include<stdio.h>
int main()
{
    struct employee
    {
        char name[10];
        int ssn;
        char gender[1];
        float salary;
        char designation[40];
        char dept[20];
    };
    struct employee e1;
    printf("Enter the name: \n ");
    scanf("%s",e1.name);
    printf("Enter the ssn of employee: \n");
    scanf("%d",&e1.ssn);
    printf("Enter employee designation: \n");
    scanf("%s",e1.designation);
    printf("Enter employee gender: \n");
    scanf("%s",e1.gender);
    printf("Enter the salary of employee: \n");
    scanf("%f",&e1.salary);
    printf("The employee details are: \n");
    printf("Name: %s \n",e1.name);
    printf("Designation: %s \n",e1.designation);
    printf("Gender: %s \n",e1.gender);
    printf("ssn: %d \n",e1.ssn);
    printf("Salary: %f \n",e1.salary);
    return 0;
}
